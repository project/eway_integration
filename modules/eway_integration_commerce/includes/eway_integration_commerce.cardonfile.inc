<?php

/**
 * @file
 * Commerce cardonfile integration.
 */

/**
 * Create card callback.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state.
 * @param array $payment_method
 *   Payment method.
 * @param object $card
 *   Card class.
 *
 * @return bool|mixed
 *   Return FALSE if unable to save the card.
 */
function eway_integration_commerce_cardonfile_create(array $form, array $form_state, array $payment_method, $card) {
  $configs = [
    'auth' => [
      'user' => $payment_method['settings']['api_username'],
      'pass' => $payment_method['settings']['api_password'],
    ],
    'method' => $payment_method['method_id'],
    'sandbox' => $payment_method['settings']['server'] == 'sandbox' ? TRUE : FALSE,
  ];
  if (isset($form_state['values']['credit_card']['number'])) {
    $card->cardno = $form_state['values']['credit_card']['number'];
  }
  $token_request = eway_integration_commerce_create_token_user_request($card);
  $token_response = eway_integration_api_request($token_request, $configs);

  if ($token_response && isset($token_response->Customer->TokenCustomerID)) {
    $card->remote_id = $token_response->Customer->TokenCustomerID;
    $card->status = 1;

    return $card;
  }
  else {
    $message = t('Unable to save your card data due to a server error');
    if (isset($token_response->ErrorPhases)) {
      $message = $message . ' - ' . implode(",", $token_response->ErrorPhases);
    }
    drupal_set_message($message, 'error');

    return FALSE;
  }
}

/**
 * Charge card callback.
 *
 * @param array $payment_method
 *   Payment method.
 * @param object $card
 *   Card class.
 * @param object $order
 *   Order object.
 * @param array $charge
 *   Charge details array.
 *
 * @return mixed|bool
 *   Return True if the payment is processed successfully.
 */
function eway_integration_commerce_cardonfile_charge(array $payment_method, $card, $order, array $charge = []) {
  $configs = [
    'auth' => [
      'user' => $payment_method['settings']['api_username'],
      'pass' => $payment_method['settings']['api_password'],
    ],
    'method' => $payment_method['method_id'],
    'sandbox' => $payment_method['settings']['server'] == 'sandbox' ? TRUE : FALSE,
  ];

  $token_request = eway_integration_commerce_prepare_token_payment($card, $order, $charge);
  $token_response = eway_integration_api_request($token_request, $configs);

  // Set current payment method id.
  // @todo Get method_id from $payment_method?
  $method_id = 'eway_dc';

  // Form a transaction object.
  $transaction = commerce_payment_transaction_new($method_id, $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->remote_id = (string) isset($token_response->TransactionID) ? $token_response->TransactionID : '';
  $transaction->remote_status = (string) isset($token_response->ResponseCode) ? $token_response->ResponseCode : '';
  $transaction->payload = print_r($token_response, TRUE);

  if ($token_response && isset($token_response->status)) {
    if ($token_response->TransactionStatus || $$token_response->ResponseCode == '00') {
      $message = t('Payment successful: @status', ['@status' => implode(", ", $token_response->status)]);
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    }
    else {
      $message = t('Payment failed: @status', ['@status' => implode(", ", $token_response->status)]);
      drupal_set_message($message, 'error');
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    }
  }
  else {
    if (isset($token_response->ErrorPhases)) {
      $message = t('Payment failed due to eWAY connection issues: @error', ['@error' => implode(", ", $token_response->ErrorPhases)]);
    }
    else {
      $message = t('Payment failed due to eWAY connection issues');
    }
    drupal_set_message($message, 'error');
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  }

  $transaction->message = $message;
  commerce_payment_transaction_save($transaction);

  return $transaction->status == COMMERCE_PAYMENT_STATUS_SUCCESS ? TRUE : FALSE;
}

/**
 * Update card callback.
 *
 * @return bool
 *   Return callback status.
 */
function eway_integration_commerce_cardonfile_update() {
  return TRUE;
}

/**
 * Delete card callback.
 *
 * @return bool
 *   Return callback status.
 */
function eway_integration_commerce_cardonfile_delete() {
  return TRUE;
}

/**
 * Alter commerce_cardonfile_checkout_pane_form.
 *
 * @param array $payment
 *   The payment details form array.
 * @param array $form
 *   Form array.
 */
function eway_integration_commerce_commerce_cardonfile_checkout_pane_form_alter(array $payment, array $form) {
  // If cardonfile module loaded, reset fields in payment details.
  if (isset($form['commerce_payment']['payment_details']['cardonfile'])) {
    if (isset($form['commerce_payment']['payment_details']['cardonfile']['#value']) && $form['commerce_payment']['payment_details']['cardonfile']['#value'] == 'new') {
      return;
    }
    // Move set default checkbox under security code field.
    $temp_field = $form['commerce_payment']['payment_details']['cardonfile_instance_default'];
    unset($form['commerce_payment']['payment_details']['cardonfile_instance_default']);
    // Add a security code which is required by eWAY API.
    $form['commerce_payment']['payment_details']['code'] = [
      '#type' => 'textfield',
      '#title' => t('CVN code'),
      '#default_value' => '',
      '#attributes' => ['autocomplete' => 'off'],
      '#maxlength' => 4,
      '#size' => 4,
      '#states' => [
        'invisible' => [
          ':input[name$="[payment_method]"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name$="[cardonfile]"]' => ['!value' => 'new'],
        ],
        'disabled' => [
          ':input[name$="[cardonfile]"]' => ['value' => 'new'],
        ],
      ],
    ];
    // eWAY integration - Client Side Encryption.
    $form['commerce_payment']['payment_details']['eway_encrypt_code'] = [
      '#type' => 'hidden',
      '#default_value' => '',
    ];
    $form['commerce_payment']['payment_details']['cardonfile_instance_default'] = $temp_field;
  }
}
