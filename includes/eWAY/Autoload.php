<?php

namespace eWAY\Autoload;

/**
 * ClassLoader implements a PSR-0 class loader.
 */
class ClassLoader {

  private $prefixLengthsPsr4 = [];
  private $prefixDirsPsr4 = [];
  private $fallbackDirsPsr4 = [];

  private $prefixesPsr0 = [];
  private $fallbackDirsPsr0 = [];

  private $useIncludePath = FALSE;
  private $classMap = [];

  /**
   * Get prefixes.
   */
  public function getPrefixes() {
    return call_user_func_array('array_merge', $this->prefixesPsr0);
  }

  /**
   * Get prefixes psr4.
   */
  public function getPrefixesPsr4() {
    return $this->prefixDirsPsr4;
  }

  /**
   * Get fallback paths.
   */
  public function getFallbackDirs() {
    return $this->fallbackDirsPsr0;
  }

  /**
   * Get Psr4 fallback paths.
   */
  public function getFallbackDirsPsr4() {
    return $this->fallbackDirsPsr4;
  }

  /**
   * Get class map.
   */
  public function getClassMap() {
    return $this->classMap;
  }

  /**
   * Add class map.
   */
  public function addClassMap(array $classMap) {
    if ($this->classMap) {
      $this->classMap = array_merge($this->classMap, $classMap);
    }
    else {
      $this->classMap = $classMap;
    }
  }

  /**
   * Add load.
   */
  public function add($prefix, $paths, $prepend = FALSE) {
    if (!$prefix) {
      if ($prepend) {
        $this->fallbackDirsPsr0 = array_merge(
          (array) $paths,
          $this->fallbackDirsPsr0
        );
      }
      else {
        $this->fallbackDirsPsr0 = array_merge(
          $this->fallbackDirsPsr0,
          (array) $paths
        );
      }

      return;
    }

    $first = $prefix[0];
    if (!isset($this->prefixesPsr0[$first][$prefix])) {
      $this->prefixesPsr0[$first][$prefix] = (array) $paths;

      return;
    }
    if ($prepend) {
      $this->prefixesPsr0[$first][$prefix] = array_merge(
        (array) $paths,
        $this->prefixesPsr0[$first][$prefix]
      );
    }
    else {
      $this->prefixesPsr0[$first][$prefix] = array_merge(
        $this->prefixesPsr0[$first][$prefix],
        (array) $paths
      );
    }
  }

  /**
   * Add psr4 path.
   */
  public function addPsr4($prefix, $paths, $prepend = FALSE) {
    if (!$prefix) {
      // Register directories for the root namespace.
      if ($prepend) {
        $this->fallbackDirsPsr4 = array_merge(
          (array) $paths,
          $this->fallbackDirsPsr4
        );
      }
      else {
        $this->fallbackDirsPsr4 = array_merge(
          $this->fallbackDirsPsr4,
          (array) $paths
        );
      }
    }
    elseif (!isset($this->prefixDirsPsr4[$prefix])) {
      // Register directories for a new namespace.
      $length = strlen($prefix);
      if ('\\' !== $prefix[$length - 1]) {
        throw new \InvalidArgumentException("A non-empty PSR-4 prefix must end with a namespace separator.");
      }
      $this->prefixLengthsPsr4[$prefix[0]][$prefix] = $length;
      $this->prefixDirsPsr4[$prefix] = (array) $paths;
    }
    elseif ($prepend) {
      // Prepend directories for an already registered namespace.
      $this->prefixDirsPsr4[$prefix] = array_merge(
        (array) $paths,
        $this->prefixDirsPsr4[$prefix]
      );
    }
    else {
      // Append directories for an already registered namespace.
      $this->prefixDirsPsr4[$prefix] = array_merge(
        $this->prefixDirsPsr4[$prefix],
        (array) $paths
      );
    }
  }

  /**
   * Set path.
   */
  public function set($prefix, $paths) {
    if (!$prefix) {
      $this->fallbackDirsPsr0 = (array) $paths;
    }
    else {
      $this->prefixesPsr0[$prefix[0]][$prefix] = (array) $paths;
    }
  }

  /**
   * Set Psr4 path.
   */
  public function setPsr4($prefix, $paths) {
    if (!$prefix) {
      $this->fallbackDirsPsr4 = (array) $paths;
    }
    else {
      $length = strlen($prefix);
      if ('\\' !== $prefix[$length - 1]) {
        throw new \InvalidArgumentException("A non-empty PSR-4 prefix must end with a namespace separator.");
      }
      $this->prefixLengthsPsr4[$prefix[0]][$prefix] = $length;
      $this->prefixDirsPsr4[$prefix] = (array) $paths;
    }
  }

  /**
   * Set include path.
   */
  public function setUseIncludePath($useIncludePath) {
    $this->useIncludePath = $useIncludePath;
  }

  /**
   * Get include path.
   */
  public function getUseIncludePath() {
    return $this->useIncludePath;
  }

  /**
   * Register class.
   */
  public function register($prepend = FALSE) {
    spl_autoload_register([$this, 'loadClass'], TRUE, $prepend);
  }

  /**
   * Unregister path.
   */
  public function unregister() {
    spl_autoload_unregister([$this, 'loadClass']);
  }

  /**
   * Load class.
   */
  public function loadClass($class) {
    if ($file = $this->findFile($class)) {
      include $file;

      return TRUE;
    }
  }

  /**
   * Find file.
   */
  public function findFile($class) {
    // Work around for PHP 5.3.0 - 5.3.2 https://bugs.php.net/50731
    if ('\\' == $class[0]) {
      $class = substr($class, 1);
    }

    // Class map lookup.
    if (isset($this->classMap[$class])) {
      return $this->classMap[$class];
    }

    // PSR-4 lookup.
    $logicalPathPsr4 = strtr($class, '\\', DIRECTORY_SEPARATOR) . '.php';

    $first = $class[0];
    if (isset($this->prefixLengthsPsr4[$first])) {
      foreach ($this->prefixLengthsPsr4[$first] as $prefix => $length) {
        if (0 === strpos($class, $prefix)) {
          foreach ($this->prefixDirsPsr4[$prefix] as $dir) {
            if (file_exists($file = $dir . DIRECTORY_SEPARATOR . substr($logicalPathPsr4, $length))) {
              return $file;
            }
          }
        }
      }
    }

    // PSR-4 fallback dirs.
    foreach ($this->fallbackDirsPsr4 as $dir) {
      if (file_exists($file = $dir . DIRECTORY_SEPARATOR . $logicalPathPsr4)) {
        return $file;
      }
    }

    // PSR-0 lookup.
    if (FALSE !== $pos = strrpos($class, '\\')) {
      // Namespaced class name.
      $logicalPathPsr0 = substr($logicalPathPsr4, 0, $pos + 1)
        . strtr(substr($logicalPathPsr4, $pos + 1), '_', DIRECTORY_SEPARATOR);
    }
    else {
      // PEAR-like class name.
      $logicalPathPsr0 = strtr($class, '_', DIRECTORY_SEPARATOR) . '.php';
    }

    if (isset($this->prefixesPsr0[$first])) {
      foreach ($this->prefixesPsr0[$first] as $prefix => $dirs) {
        if (0 === strpos($class, $prefix)) {
          foreach ($dirs as $dir) {
            if (file_exists($file = $dir . DIRECTORY_SEPARATOR . $logicalPathPsr0)) {
              return $file;
            }
          }
        }
      }
    }

    // PSR-0 fallback dirs.
    foreach ($this->fallbackDirsPsr0 as $dir) {
      if (file_exists($file = $dir . DIRECTORY_SEPARATOR . $logicalPathPsr0)) {
        return $file;
      }
    }

    // PSR-0 include paths.
    if ($this->useIncludePath && $file = stream_resolve_include_path($logicalPathPsr0)) {
      return $file;
    }

    // Remember that this class does not exist.
    return $this->classMap[$class] = FALSE;
  }

}
