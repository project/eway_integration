<?php

namespace eWAY\Payment;

/**
 * Class Collection.
 *
 * @package eWAY\Payment
 */
class Collection {

  public $Customer;

  public $ShippingAddress;

  public $Items;

  public $Payment;

}
