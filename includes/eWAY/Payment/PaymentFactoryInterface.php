<?php

namespace eWAY\Payment;

/**
 * Interface PaymentFactoryInterface.
 *
 * @package eWAY\Payment
 */
interface PaymentFactoryInterface {

  /**
   * Create payment.
   *
   * @param array $payment
   *   Payment array.
   * @param array $configs
   *   Configs array.
   *
   * @return mixed
   *   Return mixed.
   */
  public static function createPayment(array $payment, array $configs);

}
