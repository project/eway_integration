<?php

namespace eWAY\Payment;

/**
 * Class Payment.
 *
 * @package eWAY\Payment
 */
class Payment extends AbstractPayment implements PaymentInterface {

  private $data;
  private $path;

  /**
   * Payment constructor.
   *
   * @param array $payment
   *   Payment array.
   * @param array $configs
   *   Configs array.
   */
  public function __construct(array $payment, array $configs = []) {
    $this->data = new Collection();
    $this->createPath($configs);
    $this->setPayment($payment);
  }

  /**
   * Get path.
   *
   * @return string
   *   Return path.
   */
  public function getPath() {
    return '/' . ltrim($this->path, '/');
  }

  /**
   * Set path.
   *
   * @param string $path
   *   Path.
   *
   * @return $this
   *   Return object.
   */
  public function setPath($path) {
    $this->path = $path;

    return $this;
  }

  /**
   * Get payment.
   *
   * @return \eWAY\Payment\Collection|null
   *   Return payment.
   */
  public function getPayment() {
    return isset($this->data) ? $this->data : NULL;
  }

  /**
   * Set payment.
   *
   * @param array $payment
   *   Payment array.
   */
  public function setPayment(array $payment) {
    $this->setCustomer($payment['Customer']);
    $this->setShippingAddress($payment['ShippingAddress']);
    $this->setItems($payment['Items']);
    $this->setPay($payment['Payment']);
    $this->setOptions($payment['Options']);
    $this->data->Customer = $this->getCustomer();
    $this->data->ShippingAddress = $this->getShippingAddress();
    $this->data->Items = $this->getItems();
    $this->data->Payment = $this->getPay();
    $options = $this->getOptions();
    foreach ($options as $k => $v) {
      $this->data->$k = $v;
    }
  }

  /**
   * Create path.
   *
   * @param array $configs
   *   Configs array.
   */
  protected function createPath(array $configs) {
    $mode = $configs['method'];
    $path = '/';
    switch ($mode) {
      case 'eway_dc':
        $path .= 'Transaction';
        break;
    }
    $this->setPath($path);
  }

}
