<?php

namespace eWAY\Payment;

/**
 * Class PaymentFactory.
 *
 * @package eWAY\Payment
 */
class PaymentFactory implements PaymentFactoryInterface {

  /**
   * Create payment.
   *
   * @param array $payment
   *   Payment array.
   * @param array $configs
   *   Configs array.
   *
   * @return \eWAY\Payment\Payment
   *   Payment instance.
   */
  public static function createPayment(array $payment, array $configs) {
    return new Payment($payment, $configs);
  }

}
