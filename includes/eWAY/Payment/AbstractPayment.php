<?php

namespace eWAY\Payment;

/**
 * Class AbstractPayment.
 *
 * @package eWAY\Payment
 */
abstract class AbstractPayment {

  private $customer;
  private $shipping;
  private $items;
  private $pay;
  private $options;

  /**
   * Get customer details.
   *
   * @return null|mixed
   *   Return customer details.
   */
  public function getCustomer() {
    return isset($this->customer) ? $this->customer : NULL;
  }

  /**
   * Set customer.
   *
   * @param array $customer
   *   Customer array.
   *
   * @return $this
   *   Return customer object.
   */
  public function setCustomer(array $customer) {
    $this->customer = $customer;

    return $this;
  }

  /**
   * Get shipping address.
   *
   * @return null|mixed
   *   Return shipping address.
   */
  public function getShippingAddress() {
    return isset($this->shipping) ? $this->shipping : NULL;
  }

  /**
   * Set shipping.
   *
   * @param array $shipping
   *   Shipping details array.
   *
   * @return $this
   *   Return instance.
   */
  public function setShippingAddress(array $shipping) {
    $this->shipping = $shipping;

    return $this;
  }

  /**
   * Get items.
   *
   * @return null|mixed
   *   Items array.
   */
  public function getItems() {
    return isset($this->items) ? $this->items : NULL;
  }

  /**
   * Set items.
   *
   * @param array $items
   *   Items array.
   *
   * @return $this
   *   Return instance.
   */
  public function setItems(array $items) {
    $this->items = $items;

    return $this;
  }

  /**
   * Get payment details.
   *
   * @return null|mixed
   *   Return payment array.
   */
  public function getPay() {
    return isset($this->pay) ? $this->pay : NULL;
  }

  /**
   * Set payment details.
   *
   * @param array $pay
   *   Payment details array.
   *
   * @return $this
   *   Return instance.
   */
  public function setPay(array $pay) {
    $this->pay = $pay;

    return $this;
  }

  /**
   * Get options.
   *
   * @return null|mixed
   *   Options array.
   */
  public function getOptions() {
    return isset($this->options) ? $this->options : NULL;
  }

  /**
   * Set options.
   *
   * @param array $options
   *   Options array.
   *
   * @return $this
   *   Return instance.
   */
  public function setOptions(array $options) {
    $this->options = $options;

    return $this;
  }

  /**
   * Get option.
   *
   * @param string $key
   *   Option key.
   *
   * @return null|mixed
   *   Return option.
   */
  public function getOption($key) {
    return isset($this->options[$key]) ? $this->options[$key] : NULL;
  }

  /**
   * Set single option.
   *
   * @param string $key
   *   Option key.
   * @param mixed $value
   *   Option value.
   *
   * @return $this
   *   Return instance.
   */
  public function setOption($key, $value) {
    $this->options[$key] = $value;

    return $this;
  }

}
