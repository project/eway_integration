<?php

namespace eWAY\Payment;

/**
 * Interface PaymentInterface.
 *
 * @package eWAY\Payment
 */
interface PaymentInterface {

  /**
   * Get payment path.
   *
   * @return mixed
   *   Get path.
   */
  public function getPath();

  /**
   * Set payment path.
   *
   * @param string $path
   *   Set path.
   */
  public function setPath($path);

  /**
   * Get payment.
   *
   * @return mixed
   *   Return payment details.
   */
  public function getPayment();

  /**
   * Set payment.
   *
   * @param array $payment
   *   Set payment.
   */
  public function setPayment(array $payment);

}
