<?php

namespace eWAY\Message;

/**
 * Interface ResponseInterface.
 *
 * @package eWAY\Message
 */
interface ResponseInterface {

  /**
   * Get status code.
   */
  public function getStatusCode();

  /**
   * Get reason phrase.
   */
  public function getReasonPhrase();

  /**
   * List json.
   *
   * @param array $config
   *   Config array.
   *
   * @return mixed
   *   Return json.
   */
  public function json(array $config = array());

}
