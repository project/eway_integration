<?php

namespace eWAY\Message;

/**
 * Interface RequestInterface.
 *
 * @package eWAY\Message
 */
interface RequestInterface {

  /**
   * Set method.
   *
   * @param string $method
   *   Method.
   *
   * @return mixed
   *   Current instance.
   */
  public function setMethod($method);

  /**
   * Get method.
   *
   * @return mixed
   *   Method.
   */
  public function getMethod();

  /**
   * Set sandbox.
   */
  public function setSandbox($sandbox);

  /**
   * Get sandbox.
   */
  public function getSandbox();

  /**
   * Set configs.
   */
  public function setConfigs($configs);

  /**
   * Get configs.
   */
  public function getConfigs();

  /**
   * Set base.
   */
  public function setBase($base);

  /**
   * Get base.
   */
  public function getBase();

  /**
   * Get path.
   */
  public function getPath();

}
