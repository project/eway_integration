<?php

namespace eWAY\Message;

use eWAY\Payment\PaymentFactory;

/**
 * Class Request.
 *
 * @package eWAY\Message
 */
class Request implements RequestInterface {

  private $sandbox;
  private $method;
  private $base;
  private $path;
  private $payment;
  private $configs;

  /**
   * Request constructor.
   *
   * @param string $method
   *   Method.
   * @param array $payment
   *   Payment details.
   * @param array $configs
   *   Config details.
   */
  public function __construct($method, array $payment, array $configs = []) {
    $this->method = strtoupper($method);
    $this->sandbox = isset($configs['sandbox']) ? (bool) $configs['sandbox'] : FALSE;
    $this->setConfigs($configs);
    $this->base = $this->getBase();
    $pay = PaymentFactory::createPayment($payment, $this->configs);
    $this->payment = $pay->getPayment();
    $this->path = $pay->getPath();
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    return $this->method;
  }

  /**
   * {@inheritdoc}
   */
  public function setMethod($method) {
    $this->method = strtoupper($method);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSandbox($sandbox = FALSE) {
    $this->sandbox = (bool) $sandbox;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSandbox() {
    return isset($this->sandbox) ? (bool) $this->sandbox : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigs($configs) {
    $default = [
      'sandbox' => FALSE,
    ];
    $this->configs = array_merge($default, $configs);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigs() {
    return is_array($this->configs) ? $this->configs : [];
  }

  /**
   * {@inheritdoc}
   */
  public function setBase($base) {
    $this->base = $base;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBase() {
    return isset($this->base) ? $this->base : $this->setDefaultBase();
  }

  /**
   * Set default API base.
   *
   * @return mixed|string
   *   Default base.
   */
  private function setDefaultBase() {
    $sandbox = $this->getSandbox();
    if ($sandbox === TRUE) {
      $this->base = 'https://api.sandbox.ewaypayments.com';
    }
    else {
      $this->base = 'https://api.ewaypayments.com';
    }

    return $this->base;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return isset($this->path) ? $this->path : '/';
  }

  /**
   * {@inheritdoc}
   */
  public function getPayment() {
    return isset($this->payment) ? $this->payment : NULL;
  }

}
