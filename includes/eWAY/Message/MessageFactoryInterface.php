<?php

namespace eWAY\Message;

/**
 * Interface MessageFactoryInterface.
 *
 * @package eWAY\Message
 */
interface MessageFactoryInterface {

  /**
   * Create response.
   */
  public function createResponse(
    $statusCode,
    array $headers = [],
    $body = NULL,
    array $options = []
  );

  /**
   * Create request.
   */
  public function createRequest($method, $payment, array $configs = []);

}
