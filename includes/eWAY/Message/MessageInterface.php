<?php

namespace eWAY\Message;

/**
 * Interface MessageInterface.
 *
 * @package eWAY\Message
 */
interface MessageInterface {

  /**
   * Helper function.
   */
  public function __toString();

  /**
   * Set body.
   */
  public function setBody($body = NULL);

  /**
   * Get body.
   */
  public function getBody();

  /**
   * Get headers.
   */
  public function getHeaders();

  /**
   * Get header.
   */
  public function getHeader($header, $asArray = FALSE);

  /**
   * Check if has header.
   */
  public function hasHeader($header);

  /**
   * Remove header.
   */
  public function removeHeader($header);

  /**
   * Add header.
   */
  public function addHeader($header, $value);

  /**
   * Add headers.
   */
  public function addHeaders(array $headers);

  /**
   * Set header.
   */
  public function setHeader($header, $value);

  /**
   * Set headers.
   */
  public function setHeaders(array $headers);

}
