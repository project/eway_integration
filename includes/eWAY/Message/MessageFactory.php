<?php

namespace eWAY\Message;

/**
 * Class MessageFactory.
 *
 * @package eWAY\Message
 */
class MessageFactory implements MessageFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createResponse(
    $statusCode,
    array $headers = [],
    $body = NULL,
    array $options = []
  ) {
    return new Response($statusCode, $headers, $body, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function createRequest($method, $payment, array $configs = []) {

  }

}
