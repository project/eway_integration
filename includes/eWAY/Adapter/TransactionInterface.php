<?php

namespace eWAY\Adapter;

use eWAY\Message\ResponseInterface;

/**
 * Interface TransactionInterface.
 *
 * @package eWAY\Adapter
 */
interface TransactionInterface {

  /**
   * Get request.
   */
  public function getRequest();

  /**
   * Get response.
   */
  public function getResponse();

  /**
   * Set a response on the transaction.
   */
  public function setResponse(ResponseInterface $response);

}
