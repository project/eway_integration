<?php

namespace eWAY\Adapter;

/**
 * Interface AdapterInterface.
 *
 * @package eWAY\Adapter
 */
interface AdapterInterface {

  /**
   * Send transaction.
   */
  public function send(TransactionInterface $transaction);

}
