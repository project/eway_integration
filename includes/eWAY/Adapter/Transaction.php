<?php

namespace eWAY\Adapter;

use eWAY\Message\RequestInterface;
use eWAY\Message\ResponseInterface;

/**
 * Class Transaction.
 *
 * @package eWAY\Adapter
 */
class Transaction implements TransactionInterface {

  private $request;
  private $response;

  /**
   * Transaction constructor.
   *
   * @param \eWAY\Message\RequestInterface $request
   *   Request class.
   */
  public function __construct(RequestInterface $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequest() {
    return $this->request;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * {@inheritdoc}
   */
  public function setResponse(ResponseInterface $response) {
    $this->response = $response;
  }

}
