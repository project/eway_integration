<?php

namespace eWAY\Adapter\Curl;

use eWAY\Adapter\AdapterInterface;
use eWAY\Adapter\TransactionInterface;
use eWAY\Message\MessageFactoryInterface;

/**
 * Class CurlAdapter.
 *
 * @package eWAY\Adapter\Curl
 */
class CurlAdapter implements AdapterInterface {

  private $curlFactory;
  private $messageFactory;

  /**
   * CurlAdapter constructor.
   */
  public function __construct(MessageFactoryInterface $messageFactory,
                              array $options = []) {
    $this->messageFactory = $messageFactory;
    $this->curlFactory = new Curl();
  }

  /**
   * {@inheritdoc}
   */
  public function send(TransactionInterface $transaction) {
    if ($response = $transaction->getResponse()) {
      return $response;
    }

    $request = $transaction->getRequest();
    $configs = $request->getConfigs();
    $base = $request->getBase();
    $path = $request->getPath();
    $url = $base . $path;
    $payment = $request->getPayment();

    $factory = $this->curlFactory;
    $factory->setHeader('Content-Type', 'application/json');
    if (isset($configs['auth'])) {
      $factory->setBasicAuthentication($configs['auth']['user'], $configs['auth']['pass']);
    }

    $factory->post($url, json_encode($payment));
    $factory->close();

    if ($factory->error) {
      $response = $this->messageFactory->createResponse(
        $factory->errorCode,
        is_array($factory->responseHeaders->value()) ? $factory->responseHeaders->value() : [],
        $factory->response,
        [
          'url' => $factory->url,
        ]
      );
    }
    else {
      $response = $this->messageFactory->createResponse(
        $factory->httpStatusCode,
        is_array($factory->responseHeaders->value()) ? $factory->responseHeaders->value() : [],
        $factory->response,
        [
          'url' => $factory->url,
        ]
      );
    }

    $transaction->setResponse($response);

    return $transaction->getResponse();
  }

}
