<?php

namespace eWAY\Adapter\Curl;

/**
 * Class Curl.
 *
 * @package eWAY\Adapter\Curl
 */
class Curl {

  const VERSION = '3.6.5';
  const DEFAULT_TIMEOUT = 30;

  public $curl;
  public $id = NULL;

  public $error = FALSE;
  public $errorCode = 0;
  public $errorMessage = NULL;

  public $curlError = FALSE;
  public $curlErrorCode = 0;
  public $curlErrorMessage = NULL;

  public $httpError = FALSE;
  public $httpStatusCode = 0;
  public $httpErrorMessage = NULL;

  public $baseUrl = NULL;
  public $url = NULL;
  public $requestHeaders = NULL;
  public $responseHeaders = NULL;
  public $rawResponseHeaders = '';
  public $response = NULL;
  public $rawResponse = NULL;

  public $beforeSendFunction = NULL;
  public $downloadCompleteFunction = NULL;
  private $successFunction = NULL;
  private $errorFunction = NULL;
  private $completeFunction = NULL;

  private $cookies = [];
  private $headers = [];
  private $options = [];

  private $jsonDecoder = NULL;
  private $jsonPattern = '/^(?:application|text)\/(?:[a-z]+(?:[\.-][0-9a-z]+){0,}[\+\.]|x-)?json(?:-[a-z]+)?/i';
  private $xmlPattern = '~^(?:text/|application/(?:atom\+|rss\+)?)xml~i';

  /**
   * Construct.
   */
  public function __construct($base_url = NULL) {
    if (!extension_loaded('curl')) {
      throw new \ErrorException('cURL library is not loaded');
    }

    $this->curl = curl_init();
    $this->id = 1;
    $this->setDefaultUserAgent();
    $this->setDefaultJsonDecoder();
    $this->setDefaultTimeout();
    $this->setOpt(CURLINFO_HEADER_OUT, TRUE);
    $this->setOpt(CURLOPT_HEADERFUNCTION, [$this, 'headerCallback']);
    $this->setOpt(CURLOPT_RETURNTRANSFER, TRUE);
    $this->headers = new CaseInsensitiveArray();
    $this->setUrl($base_url);
  }

  /**
   * Before Send.
   */
  public function beforeSend($callback) {
    $this->beforeSendFunction = $callback;
  }

  /**
   * Build Post Data.
   */
  public function buildPostData($data) {
    if (is_array($data)) {
      if (self::isArrayMultidim($data)) {
        if (isset($this->headers['Content-Type']) &&
          preg_match($this->jsonPattern, $this->headers['Content-Type'])
        ) {
          $json_str = json_encode($data);
          if (!($json_str === FALSE)) {
            $data = $json_str;
          }
        }
        else {
          $data = self::httpBuildMultiQuery($data);
        }
      }
      else {
        $binary_data = FALSE;
        foreach ($data as $key => $value) {
          if (is_array($value) && empty($value)) {
            $data[$key] = '';
          }
          elseif (is_string($value) && strpos($value, '@') === 0) {
            $binary_data = TRUE;
            if (class_exists('CURLFile')) {
              $data[$key] = new \CURLFile(substr($value, 1));
            }
          }
          elseif ($value instanceof \CURLFile) {
            $binary_data = TRUE;
          }
        }

        if (!$binary_data) {
          if (isset($this->headers['Content-Type']) &&
            preg_match($this->jsonPattern, $this->headers['Content-Type'])
          ) {
            $json_str = json_encode($data);
            if (!($json_str === FALSE)) {
              $data = $json_str;
            }
          }
          else {
            $data = http_build_query($data, '', '&');
          }
        }
      }
    }

    return $data;
  }

  /**
   * Call.
   */
  public function call() {
    $args = func_get_args();
    $function = array_shift($args);
    if (is_callable($function)) {
      array_unshift($args, $this);
      call_user_func_array($function, $args);
    }
  }

  /**
   * Close.
   */
  public function close() {
    if (is_resource($this->curl)) {
      curl_close($this->curl);
    }
    $this->options = NULL;
    $this->jsonDecoder = NULL;
  }

  /**
   * Complete.
   */
  public function complete($callback) {
    $this->completeFunction = $callback;
  }

  /**
   * Progress.
   */
  public function progress($callback) {
    $this->setOpt(CURLOPT_PROGRESSFUNCTION, $callback);
    $this->setOpt(CURLOPT_NOPROGRESS, FALSE);
  }

  /**
   * Delete.
   */
  public function delete($url, $query_parameters = [], $data = []) {
    if (is_array($url)) {
      $data = $query_parameters;
      $query_parameters = $url;
      $url = $this->baseUrl;
    }

    $this->setUrl($url, $query_parameters);
    $this->setOpt(CURLOPT_CUSTOMREQUEST, 'DELETE');
    $this->setOpt(CURLOPT_POSTFIELDS, $this->buildPostData($data));

    return $this->exec();
  }

  /**
   * Download Complete.
   */
  public function downloadComplete($fh) {
    if (!$this->error && $this->downloadCompleteFunction) {
      rewind($fh);
      $this->call($this->downloadCompleteFunction, $fh);
      $this->downloadCompleteFunction = NULL;
    }

    if (is_resource($fh)) {
      fclose($fh);
    }

    if (!defined('STDOUT')) {
      define('STDOUT', fopen('php://stdout', 'w'));
    }

    $this->setOpt(CURLOPT_FILE, STDOUT);

    $this->setOpt(CURLOPT_RETURNTRANSFER, TRUE);
  }

  /**
   * Download.
   */
  public function download($url, $mixed_filename) {
    if (is_callable($mixed_filename)) {
      $this->downloadCompleteFunction = $mixed_filename;
      $fh = tmpfile();
    }
    else {
      $filename = $mixed_filename;
      $fh = fopen($filename, 'wb');
    }

    $this->setOpt(CURLOPT_FILE, $fh);
    $this->get($url);
    $this->downloadComplete($fh);

    return !$this->error;
  }

  /**
   * Error.
   */
  public function error($callback) {
    $this->errorFunction = $callback;
  }

  /**
   * Exec.
   */
  public function exec($ch = NULL) {
    if (!($ch === NULL)) {
      $this->rawResponse = curl_multi_getcontent($ch);
    }
    else {
      $this->call($this->beforeSendFunction);
      $this->rawResponse = curl_exec($this->curl);
      $this->curlErrorCode = curl_errno($this->curl);
    }

    $this->curlErrorMessage = curl_error($this->curl);
    $this->curlError = !($this->curlErrorCode === 0);
    $this->httpStatusCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
    $this->httpError = in_array(floor($this->httpStatusCode / 100), [
      4,
      5,
    ]);
    $this->error = $this->curlError || $this->httpError;
    $this->errorCode = $this->error ? ($this->curlError ? $this->curlErrorCode : $this->httpStatusCode) : 0;

    $this->requestHeaders = $this->parseRequestHeaders(curl_getinfo($this->curl, CURLINFO_HEADER_OUT));
    $this->responseHeaders = $this->parseResponseHeaders($this->rawResponseHeaders);
    list($this->response, $this->rawResponse) = $this->parseResponse($this->responseHeaders, $this->rawResponse);

    $this->httpErrorMessage = '';
    if ($this->error) {
      if (isset($this->responseHeaders['Status-Line'])) {
        $this->httpErrorMessage = $this->responseHeaders['Status-Line'];
      }
    }
    $this->errorMessage = $this->curlError ? $this->curlErrorMessage : $this->httpErrorMessage;

    if (!$this->error) {
      $this->call($this->successFunction);
    }
    else {
      $this->call($this->errorFunction);
    }

    $this->call($this->completeFunction);

    return $this->response;
  }

  /**
   * Get.
   */
  public function get($url, $data = []) {
    if (is_array($url)) {
      $data = $url;
      $url = $this->baseUrl;
    }
    $this->setUrl($url, $data);
    $this->setOpt(CURLOPT_CUSTOMREQUEST, 'GET');
    $this->setOpt(CURLOPT_HTTPGET, TRUE);

    return $this->exec();
  }

  /**
   * Get Opt.
   */
  public function getOpt($option) {
    return $this->options[$option];
  }

  /**
   * Head.
   */
  public function head($url, $data = []) {
    if (is_array($url)) {
      $data = $url;
      $url = $this->baseUrl;
    }
    $this->setUrl($url, $data);
    $this->setOpt(CURLOPT_CUSTOMREQUEST, 'HEAD');
    $this->setOpt(CURLOPT_NOBODY, TRUE);

    return $this->exec();
  }

  /**
   * Header Callback.
   */
  public function headerCallback($ch, $header) {
    $this->rawResponseHeaders .= $header;

    return strlen($header);
  }

  /**
   * Options.
   */
  public function options($url, $data = []) {
    if (is_array($url)) {
      $data = $url;
      $url = $this->baseUrl;
    }
    $this->setUrl($url, $data);
    $this->unsetHeader('Content-Length');
    $this->setOpt(CURLOPT_CUSTOMREQUEST, 'OPTIONS');

    return $this->exec();
  }

  /**
   * Patch.
   */
  public function patch($url, $data = []) {
    if (is_array($url)) {
      $data = $url;
      $url = $this->baseUrl;
    }
    $this->setUrl($url);
    $this->unsetHeader('Content-Length');
    $this->setOpt(CURLOPT_CUSTOMREQUEST, 'PATCH');
    $this->setOpt(CURLOPT_POSTFIELDS, $data);

    return $this->exec();
  }

  /**
   * Post.
   */
  public function post($url, $data = []) {
    if (is_array($url)) {
      $data = $url;
      $url = $this->baseUrl;
    }

    if (is_array($data) && empty($data)) {
      $this->unsetHeader('Content-Length');
    }

    $this->setUrl($url);
    $this->setOpt(CURLOPT_CUSTOMREQUEST, 'POST');
    $this->setOpt(CURLOPT_POST, TRUE);
    $this->setOpt(CURLOPT_POSTFIELDS, $this->buildPostData($data));

    return $this->exec();
  }

  /**
   * Put.
   */
  public function put($url, $data = []) {
    if (is_array($url)) {
      $data = $url;
      $url = $this->baseUrl;
    }
    $this->setUrl($url);
    $this->setOpt(CURLOPT_CUSTOMREQUEST, 'PUT');
    $put_data = $this->buildPostData($data);
    if (empty($this->options[CURLOPT_INFILE]) && empty($this->options[CURLOPT_INFILESIZE])) {
      $this->setHeader('Content-Length', strlen($put_data));
    }
    $this->setOpt(CURLOPT_POSTFIELDS, $put_data);

    return $this->exec();
  }

  /**
   * Set Basic Authentication.
   */
  public function setBasicAuthentication($username, $password = '') {
    $this->setOpt(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    $this->setOpt(CURLOPT_USERPWD, $username . ':' . $password);
  }

  /**
   * Set Digest Authentication.
   */
  public function setDigestAuthentication($username, $password = '') {
    $this->setOpt(CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
    $this->setOpt(CURLOPT_USERPWD, $username . ':' . $password);
  }

  /**
   * Set Cookie.
   */
  public function setCookie($key, $value) {
    $this->cookies[$key] = $value;
    $this->setOpt(CURLOPT_COOKIE, str_replace(' ', '%20', urldecode(http_build_query($this->cookies, '', '; '))));
  }

  /**
   * Set Port.
   */
  public function setPort($port) {
    $this->setOpt(CURLOPT_PORT, intval($port));
  }

  /**
   * Set Connect Timeout.
   */
  public function setConnectTimeout($seconds) {
    $this->setOpt(CURLOPT_CONNECTTIMEOUT, $seconds);
  }

  /**
   * Set Cookie File.
   */
  public function setCookieFile($cookie_file) {
    $this->setOpt(CURLOPT_COOKIEFILE, $cookie_file);
  }

  /**
   * Set Cookie Jar.
   */
  public function setCookieJar($cookie_jar) {
    $this->setOpt(CURLOPT_COOKIEJAR, $cookie_jar);
  }

  /**
   * Set Default JSON Decoder.
   */
  public function setDefaultJsonDecoder() {
    $this->jsonDecoder = function ($response) {
      $json_obj = json_decode($response, FALSE);
      if (!($json_obj === NULL)) {
        $response = $json_obj;
      }

      return $response;
    };
  }

  /**
   * Set Default Timeout.
   */
  public function setDefaultTimeout() {
    $this->setTimeout(self::DEFAULT_TIMEOUT);
  }

  /**
   * Set Default User Agent.
   */
  public function setDefaultUserAgent() {
    $user_agent = 'PHP-Curl-Class/' . self::VERSION . ' (+https://github.com/php-curl-class/php-curl-class)';
    $user_agent .= ' PHP/' . PHP_VERSION;
    $curl_version = curl_version();
    $user_agent .= ' curl/' . $curl_version['version'];
    $this->setUserAgent($user_agent);
  }

  /**
   * Set Header.
   */
  public function setHeader($key, $value) {
    $this->headers[$key] = $value;
    $headers = [];
    foreach ($this->headers as $key => $value) {
      $headers[$key] = $value;
    }
    $this->setOpt(CURLOPT_HTTPHEADER, array_map(function ($value, $key) {
      return $key . ': ' . $value;
    }, $headers, array_keys($headers)));
  }

  /**
   * Set JSON Decoder.
   */
  public function setJsonDecoder($function) {
    if (is_callable($function)) {
      $this->jsonDecoder = $function;
    }
  }

  /**
   * Set Opt.
   */
  public function setOpt($option, $value) {
    $required_options = [
      CURLINFO_HEADER_OUT => 'CURLINFO_HEADER_OUT',
      CURLOPT_RETURNTRANSFER => 'CURLOPT_RETURNTRANSFER',
    ];

    if (in_array($option, array_keys($required_options), TRUE) && !($value === TRUE)) {
      trigger_error($required_options[$option] . ' is a required option', E_USER_WARNING);
    }

    $this->options[$option] = $value;

    return curl_setopt($this->curl, $option, $value);
  }

  /**
   * Set Referer.
   */
  public function setReferer($referer) {
    $this->setReferrer($referer);
  }

  /**
   * Set Referrer.
   */
  public function setReferrer($referrer) {
    $this->setOpt(CURLOPT_REFERER, $referrer);
  }

  /**
   * Set Timeout.
   */
  public function setTimeout($seconds) {
    $this->setOpt(CURLOPT_TIMEOUT, $seconds);
  }

  /**
   * Set Url.
   */
  public function setUrl($url, $data = []) {
    $this->baseUrl = $url;
    $this->url = $this->buildUrl($url, $data);
    $this->setOpt(CURLOPT_URL, $this->url);
  }

  /**
   * Set User Agent.
   */
  public function setUserAgent($user_agent) {
    $this->setOpt(CURLOPT_USERAGENT, $user_agent);
  }

  /**
   * Success.
   */
  public function success($callback) {
    $this->successFunction = $callback;
  }

  /**
   * Unset Header.
   */
  public function unsetHeader($key) {
    $this->setHeader($key, '');
    unset($this->headers[$key]);
  }

  /**
   * Verbose.
   */
  public function verbose($on = TRUE) {
    $this->setOpt(CURLOPT_VERBOSE, $on);
  }

  /**
   * Destruct.
   */
  public function __destruct() {
    $this->close();
  }

  /**
   * Build Url.
   */
  private function buildUrl($url, $data = []) {
    return $url . (empty($data) ? '' : '?' . http_build_query($data));
  }

  /**
   * Parse Headers.
   */
  private function parseHeaders($raw_headers) {
    $raw_headers = preg_split('/\r\n/', $raw_headers, NULL, PREG_SPLIT_NO_EMPTY);
    $http_headers = new CaseInsensitiveArray();

    $raw_headers_count = count($raw_headers);
    for ($i = 1; $i < $raw_headers_count; $i++) {
      list($key, $value) = explode(':', $raw_headers[$i], 2);
      $key = trim($key);
      $value = trim($value);
      // Use isset() as array_key_exists() and ArrayAccess are not compatible.
      if (isset($http_headers[$key])) {
        $http_headers[$key] .= ',' . $value;
      }
      else {
        $http_headers[$key] = $value;
      }
    }

    return [
      isset($raw_headers['0']) ? $raw_headers['0'] : '',
      $http_headers,
    ];
  }

  /**
   * Parse Request Headers.
   */
  private function parseRequestHeaders($raw_headers) {
    $request_headers = new CaseInsensitiveArray();
    list($first_line, $headers) = $this->parseHeaders($raw_headers);
    $request_headers['Request-Line'] = $first_line;
    foreach ($headers as $key => $value) {
      $request_headers[$key] = $value;
    }

    return $request_headers;
  }

  /**
   * Parse Response.
   */
  private function parseResponse($response_headers, $raw_response) {
    $response = $raw_response;
    if (isset($response_headers['Content-Type'])) {
      if (preg_match($this->jsonPattern, $response_headers['Content-Type'])) {
        $json_decoder = $this->jsonDecoder;
        if (is_callable($json_decoder)) {
          $response = $json_decoder($response);
        }
      }
      elseif (preg_match($this->xmlPattern, $response_headers['Content-Type'])) {
        $xml_obj = @simplexml_load_string($response);
        if (!($xml_obj === FALSE)) {
          $response = $xml_obj;
        }
      }
    }

    return [$response, $raw_response];
  }

  /**
   * Parse Response Headers.
   */
  private function parseResponseHeaders($raw_response_headers) {
    $response_header_array = explode("\r\n\r\n", $raw_response_headers);
    $response_header = '';
    for ($i = count($response_header_array) - 1; $i >= 0; $i--) {
      if (stripos($response_header_array[$i], 'HTTP/') === 0) {
        $response_header = $response_header_array[$i];
        break;
      }
    }

    $response_headers = new CaseInsensitiveArray();
    list($first_line, $headers) = $this->parseHeaders($response_header);
    $response_headers['Status-Line'] = $first_line;
    foreach ($headers as $key => $value) {
      $response_headers[$key] = $value;
    }

    return $response_headers;
  }

  /**
   * Http Build Multi Query.
   */
  public static function httpBuildMultiQuery($data, $key = NULL) {
    $query = [];

    if (empty($data)) {
      return $key . '=';
    }

    $is_array_assoc = self::isArrayAssoc($data);

    foreach ($data as $k => $value) {
      if (is_string($value) || is_numeric($value)) {
        $brackets = $is_array_assoc ? '[' . $k . ']' : '[]';
        $query[] = urlencode($key === NULL ? $k : $key . $brackets) . '=' . rawurlencode($value);
      }
      elseif (is_array($value)) {
        $nested = $key === NULL ? $k : $key . '[' . $k . ']';
        $query[] = self::httpBuildMultiQuery($value, $nested);
      }
    }

    return implode('&', $query);
  }

  /**
   * Is Array Assoc.
   */
  public static function isArrayAssoc($array) {
    return (bool) count(array_filter(array_keys($array), 'is_string'));
  }

  /**
   * Is Array Multidim.
   */
  public static function isArrayMultidim($array) {
    if (!is_array($array)) {
      return FALSE;
    }

    return (bool) count(array_filter($array, 'is_array'));
  }

}

/**
 * Class CaseInsensitiveArray.
 *
 * @package eWAY\Adapter\Curl
 */
class CaseInsensitiveArray implements \ArrayAccess, \Countable, \Iterator {

  private $container = [];

  /**
   * Offset set.
   */
  public function offsetSet($offset, $value) {
    if ($offset === NULL) {
      $this->container[] = $value;
    }
    else {
      $index = array_search(strtolower($offset), array_keys(array_change_key_case($this->container, CASE_LOWER)));
      if (!($index === FALSE)) {
        $keys = array_keys($this->container);
        unset($this->container[$keys[$index]]);
      }
      $this->container[$offset] = $value;
    }
  }

  /**
   * Offset check.
   */
  public function offsetExists($offset) {
    return array_key_exists(strtolower($offset), array_change_key_case($this->container, CASE_LOWER));
  }

  /**
   * Unset offset.
   */
  public function offsetUnset($offset) {
    unset($this->container[$offset]);
  }

  /**
   * Get offset.
   */
  public function offsetGet($offset) {
    $index = array_search(strtolower($offset), array_keys(array_change_key_case($this->container, CASE_LOWER)));
    if ($index === FALSE) {
      return NULL;
    }

    $values = array_values($this->container);

    return $values[$index];
  }

  /**
   * Count.
   */
  public function count() {
    return count($this->container);
  }

  /**
   * Current.
   */
  public function current() {
    return current($this->container);
  }

  /**
   * Next.
   */
  public function next() {
    return next($this->container);
  }

  /**
   * Key.
   */
  public function key() {
    return key($this->container);
  }

  /**
   * Valid.
   */
  public function valid() {
    return !($this->current() === FALSE);
  }

  /**
   * Rewind.
   */
  public function rewind() {
    reset($this->container);
  }

  /**
   * Value.
   */
  public function value() {
    return $this->container;
  }

}
