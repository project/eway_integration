<?php

namespace eWAY;

use eWAY\Adapter\Curl\CurlAdapter;
use eWAY\Adapter\Transaction;
use eWAY\Message\Request;
use eWAY\Message\RequestInterface;
use eWAY\Message\MessageFactory;

/**
 * Class Eway.
 *
 * @package eWAY
 */
class Eway {

  private $messageFactory;
  private $adapter;

  /**
   * Eway constructor.
   *
   * @param array $config
   *   Configs array.
   */
  public function __construct(array $config = []) {
    $this->configureAdapter($config);
  }

  /**
   * Create request.
   */
  public function createRequest($method, $payment, $configs = []) {
    $request = new Request($method, $payment, $configs);

    return $request;
  }

  /**
   * Send request.
   *
   * @param \eWAY\Message\RequestInterface $request
   *   Request.
   *
   * @return bool|mixed
   *   Response.
   */
  public function send(RequestInterface $request) {
    $transaction = new Transaction($request);
    if ($response = $this->adapter->send($transaction)) {
      return $response;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Config adapter.
   *
   * @param array $config
   *   Configs array.
   */
  private function configureAdapter(array &$config) {
    if (isset($config['message_factory'])) {
      $this->messageFactory = $config['message_factory'];
    }
    else {
      $this->messageFactory = new MessageFactory();
    }
    if (isset($config['adapter'])) {
      $this->adapter = $config['adapter'];
    }
    else {
      $this->getDefaultAdapter();
    }
  }

  /**
   * Get default adapter.
   */
  private function getDefaultAdapter() {
    if (extension_loaded('curl')) {
      $this->adapter = new CurlAdapter($this->messageFactory);
    }
    else {
      throw new \RuntimeException('eWAY integration requires cURL.');
    }
  }

}
