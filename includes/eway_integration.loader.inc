<?php

namespace eWAY;

use eWAY\Autoload\ClassLoader;

/**
 * Class Loader.
 */
class Loader {

  private static $loader;

  /**
   * Load classes.
   *
   * @param string $class
   *   Class name.
   */
  public static function loadClassLoader($class) {
    if ('eWAY\Autoload\ClassLoader' === $class) {
      require __DIR__ . '/eWAY/Autoload.php';
    }
  }

  /**
   * Get loader object.
   *
   * @return \eWAY\Autoload\ClassLoader
   *   Return loader object.
   */
  public static function getLoader() {
    if (NULL !== self::$loader) {
      return self::$loader;
    }

    spl_autoload_register([
      'eWAY\Loader',
      'loadClassLoader',
    ], TRUE, TRUE);
    self::$loader = $loader = new ClassLoader();
    spl_autoload_unregister(['Loader', 'loadClassLoader']);

    $loader->set('eWAY', __DIR__ . '/eWAY');
    $loader->setPsr4('eWAY\\', __DIR__ . '/eWAY');
    $loader->register(TRUE);
    $loader->setUseIncludePath(TRUE);

    return $loader;
  }

}

return Loader::getLoader();
